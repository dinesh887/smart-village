/**
 * Created by dineshr on 10/7/2015.
 */
angular.module("socially").controller("deviceList", ['$scope', '$meteor', '$rootScope', '$state', '$mdDialog','DataService','$mdMedia',
    function($scope, $meteor, $rootScope, $state, $mdDialog, DataService, $mdMedia) {
        $scope.page = 1;
        $scope.perPage = 3;
        $scope.orderProperty = '1';

        $scope.devices = $meteor.collection(function() {
            return Device.find({});
        });

        $scope.showAdvanced = function(ev, dev) {

            $mdDialog.show({
                controller: AdvanceController,
                templateUrl: 'client/parties/views/device.advance.modal.ng.html',
                parent: angular.element(document.body),
                clickOutsideToClose:true,
                fullscreen: $mdMedia('sm') && $scope.customFullscreen,
                locals : {
                    device :dev
                }
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
            $scope.$watch(function() {
                return $mdMedia('sm');
            }, function(sm) {
                $scope.customFullscreen = (sm === true);
            });

        };

        function AdvanceController($scope, $mdDialog, device, $timeout, $q, $meteor) {
  /*+++++++++++++++++++++++++ TAB 1 ++++++++++++++++++++++++++++++++++++*/          
            var deviceData = $meteor.collection(function() {
                return DeviceData.find({'mac':device.mac});
            });
            $scope.dev = deviceData[0];


 /*+++++++++++++++++++++++++ TAB 2 ++++++++++++++++++++++++++++++++++++*/
            $scope.simulateQuery = false;
            $scope.isDisabled    = false;
            $scope.repos         = loadAll();
            $scope.querySearch   = querySearch;
            $scope.selectedItemChange = selectedItemChange;
            $scope.searchTextChange   = searchTextChange;
            $scope.addedList = [];

            function querySearch (query) {
                var results = query ? $scope.repos.filter( createFilterFor(query) ) : $scope.repos,
                    deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            }
            function searchTextChange(text) {

            }
            function selectedItemChange(item) {
                if(item != undefined){
                    $scope.addedList.push(item);

                }

            }
            /**
             * Build `components` list of key/value pairs
             */
            function loadAll() {
                var repos = $meteor.collection(Meteor.users,false).subscribe('usersList');
                return repos.map( function (repo) {
                    repo.value = repo.email;
                    return repo;
                });
            }
            /**
             * Create filter function for a query string
             */
            function createFilterFor(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(item) {
                    return (item.value.indexOf(lowercaseQuery) === 0);
                };
            }




/*+++++++++++++++++++++++++ TAB 3 ++++++++++++++++++++++++++++++++++++*/
             var deviceDatalg = $meteor.collection(function() {
                return DeviceDataLog.find({'mac':device.mac});
            });


              $scope.chartObject = {};

        $scope.secondRow = [
            {v: new Date("October 13, 2014 11:15:00")},
            {v: 13}
        ];


        $scope.chartObject.type = "AnnotationChart";

        $scope.chartObject.data = {"cols": [
            {id: "month", label: "Month", type: "date"},
            {id: "kepler-data", label: "Kepler-22b mission", type: "number"},
            {id: "kepler-annot", label: "Kepler-22b Annotation Title", type: "string"},
            {id: "kepler-annot-body", label: "Kepler-22b Annotation Text", type: "string"},
            {id: "desktop-data", label: "Gliese mission", type: "number"},
            {id: "desktop-annot", label: "Gliese Annotation Title", type: "string"},
            {id: "desktop-annot-body", label: "Gliese Annotaioon Text", type: "string"}
        ], "rows": [
        ]};

        _.each(deviceDatalg,function(data){
                    if(data) {
                        $scope.chartObject.data.rows.push({
                            c: [
                                {v: data.timeStamp},
                                {v: parseInt(data.pm[0].kwh_p)},
                                {v: 'kwh-Positive'},
                                {v: undefined},
                                {v: parseInt(data.pm[0].kwh_n)},
                                {v: 'kwh-Negative'},
                                {v: undefined}
                            ]
                        }) 
                    }
        });

        $scope.chartObject.options = {
            displayAnnotations: true
        };

/*+++++++++++++++++++++++++ Button Action ++++++++++++++++++++++++++++++++++++*/
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }

        $meteor.autorun($scope, function() {
            $meteor.subscribe('devices', {
                limit: parseInt($scope.getReactively('perPage')),
                skip: (parseInt($scope.getReactively('page')) - 1) * parseInt($scope.getReactively('perPage'))

            }, $scope.getReactively('search')).then(function() {
                /*$scope.devices.forEach( function (party) {
                 party.onClicked = function () {
                 $state.go('partyDetails', {partyId: party._id});
                 };
                 });*/
            });

            $meteor.subscribe('deviceData', {}).then(function() {});
            $meteor.subscribe('deviceDataLog', {}).then(function() {});


            var styles1 = [{
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#e0efef"}]
            }, {
                "featureType": "poi",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"hue": "#1900ff"}, {"color": "#c0e8e8"}]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{"lightness": 100}, {"visibility": "simplified"}]
            }, {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [{"visibility": "on"}, {"lightness": 700}]
            }, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#7dcdcd"}]}];
            var styles2 = [{
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#444444"}]
            }, {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [{"color": "#f2f2f2"}]
            }, {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "road",
                "elementType": "all",
                "stylers": [{"saturation": -100}, {"lightness": 45}]
            }, {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [{"visibility": "simplified"}]
            }, {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "water",
                "elementType": "all",
                "stylers": [{"color": "#46bcec"}, {"visibility": "on"}]
            }];


            $scope.map = {
                center: {
                    latitude: 6.933,
                    longitude: 80.305
                },
                options: {
                    styles: styles2,
                    maxZoom: 10
                },
                zoom: 8
            };
        });
    }]);

