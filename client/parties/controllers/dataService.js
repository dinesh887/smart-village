/**
 * Created by Heshanr on 10/8/2015.
 */
angular.module("socially").service("DataService",[
    function() {

        var _getDeviceList = function (params,callback) {
            var rqst = '/street_light.get_dev_list';
            if(params){
                rqst = '/street_light.get_dev_list?';
                _.map(params,function(v,k){
                    rqst += ''+k+'='+v+'&'
                });
            }


            processRequest(rqst ,callback)
        };







        var processRequest = function(req,callback){
            Meteor.call('get',req,function(err,response){
                callback(err,response);
            });
        };

        return {
            getDeviceList:_getDeviceList
        }
    }]);