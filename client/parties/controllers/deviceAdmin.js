/**
 * Created by dineshpriyashantha on 1/3/16.
 */
angular.module("socially").controller("deviceListAdmin", ['$scope', '$meteor', '$rootScope', '$state', '$mdDialog','DataService','$mdMedia',
    function($scope, $meteor, $rootScope, $state, $mdDialog, DataService, $mdMedia) {
        $scope.page = 1;
        $scope.perPage = 3;
        $scope.orderProperty = '1';
        $scope.add_open = true;

        $scope.devices = $meteor.collection(function() {
            return Device.find({});
        });

        $scope.showAdvanced = function(ev, dev) {

            $mdDialog.show({
                controller: dev?AdvanceController:GlobalConfigController,
                templateUrl: dev?'client/parties/views/device.admin.modal.ng.html' : 'client/parties/views/device.admin.config.modal.ng.html',
                parent: angular.element(document.body),
                clickOutsideToClose:true,
                fullscreen: $mdMedia('sm') && $scope.customFullscreen,
                locals : {
                    device :dev
                }
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
            $scope.$watch(function() {
                return $mdMedia('sm');
            }, function(sm) {
                $scope.customFullscreen = (sm === true);
            });

        };

        $scope.addDevice = function(){
            if($scope.mac != ''){
                var reqst = '/add_dev_by_mac?mac='+$scope.mac;
                $meteor.call('get', reqst, function(err,response){
                    console.log('data recieved : '+response);
                    var resp = JSON.parse(response.content);
                    if(resp.success){
                        $scope.add_open = true;
                    }else{
                        console.log('++++++++++++++++++ERROR Occurd while adding++++++++++++++');
                    }

                });
            }
            
        };

        $scope.removeDevice = function(dev){
            var confirm = $mdDialog.confirm()
            .title('Are you sure, You want to remove this device?')
            .textContent('')
            .ariaLabel('remove device')
            .targetEvent(ev)
            .ok('Yes')
            .cancel('No');
            $mdDialog.show(confirm).then(function() {
                var reqst = '/kick?name='+dev.devId;
                $meteor.call('get', reqst, function(err,response){
                    console.log('data recieved : '+response);
                    var resp = JSON.parse(response.content);
                    if(resp.success){
                        $scope.add_open = true;
                    }else{
                        console.log('-----------------ERROR Occurd while removing---------------');
                    }

                });
            }, function() {
                
            });
            
        };


        function AdvanceController($scope, $mdDialog, device, $timeout, $q, $meteor) {
            /*+++++++++++++++++++++++++ TAB 1 ++++++++++++++++++++++++++++++++++++*/
            var deviceData = $meteor.collection(function() {
                return DeviceData.find({'mac':device.mac});
            });
            $scope.dev = deviceData[0];


            /*+++++++++++++++++++++++++ TAB 2 ++++++++++++++++++++++++++++++++++++*/
            $scope.simulateQuery = false;
            $scope.isDisabled    = false;
            $scope.repos         = loadAll();
            $scope.querySearch   = querySearch;
            $scope.selectedItemChange = selectedItemChange;
            $scope.searchTextChange   = searchTextChange;
            $scope.addedList = [];

            function querySearch (query) {
                var results = query ? $scope.repos.filter( createFilterFor(query) ) : $scope.repos,
                    deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            }
            function searchTextChange(text) {

            }
            function selectedItemChange(item) {
                if(item != undefined){
                    $scope.addedList.push(item);

                }

            }
            /**
             * Build `components` list of key/value pairs
             */
            function loadAll() {
                var repos = $meteor.collection(Meteor.users,false).subscribe('usersList');
                return repos.map( function (repo) {
                    repo.value = repo.email;
                    return repo;
                });
            }
            /**
             * Create filter function for a query string
             */
            function createFilterFor(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(item) {
                    return (item.value.indexOf(lowercaseQuery) === 0);
                };
            }

            /*+++++++++++++++++++++++++ TAB 3 +++++++++++++++++++++++++++++++++++++*/
            $scope.config = {};
            $scope.config.dimLevel  = $scope.dev.l[0];
            $scope.config.rtc  = new Date();
            $scope.setDimLevel = function(){
                var reqst = '/street_light.set_dim_level?mac='+device.mac+'&level='+$scope.config.dimLevel;
                $meteor.call('get', reqst, function(err,response){
                    console.log('data recieved : '+response);
                    var resp = JSON.parse(response.content);
                    if(resp.success){
                        $scope.add_open = true;
                    }else{
                        console.log('++++++++++++++++++ERROR Occurd while Set Dim level++++++++++++++');
                    }

                });
            };

            $scope.setRTC = function(){
                var date = ""+ ($scope.config.rtc.getFullYear()+"").slice(2,4)+"-"+$scope.config.rtc.getMonth()+"-"+$scope.config.rtc.getDate()+"-"+$scope.config.rtc.getHours()+"-"+$scope.config.rtc.getMinutes()+"-"+$scope.config.rtc.getSeconds();
                var reqst = '/street_light.set_rtc?mac='+device.mac+'&rtc='+date;
                $meteor.call('get', reqst, function(err,response){
                    console.log('data recieved : '+response);
                    var resp = JSON.parse(response.content);
                    if(resp.success){
                        $scope.add_open = true;
                    }else{
                        console.log('++++++++++++++++++ERROR Occurd while Set RTC ++++++++++++++');
                    }

                });
            };



            /*+++++++++++++++++++++++++ TAB 4 ++++++++++++++++++++++++++++++++++++*/
            var chart1 = {};
            chart1.type = "PieChart";
            chart1.data = [
                ['Component', 'cost'],
                ['Software', 50000],
                ['Hardware', 80000]
            ];
            chart1.data.push(['Services',20000]);
            chart1.options = {
                displayExactValues: true,
                width: 400,
                height: 200,
                is3D: true,
                chartArea: {left:10,top:10,bottom:0,height:"100%"}
            };

            chart1.formatters = {
                number : [{
                    columnNum: 1,
                    pattern: "$ #,##0.00"
                }]
            };

            $scope.chart = chart1;

            $scope.aa=1*$scope.chart.data[1][1];
            $scope.bb=1*$scope.chart.data[2][1];
            $scope.cc=1*$scope.chart.data[3][1];

            /*+++++++++++++++++++++++++ TAB 5 +++++++++++++++++++++++++++++++++++++++++++++++*/
            $scope.inputData = [
                {time:0, level:0},
                {time:0, level:0},
                {time:0, level:0},
                {time:0, level:0},
                {time:0, level:0},
                {time:0, level:0},
                {time:0, level:0},
                {time:0, level:0},
                {time:0, level:0},
                {time:0, level:0}
            ];

            $scope.eventSources = [];
            $scope.calendarConfig = {
                lang: 'fr',
                contentHeight: 'auto',
                selectable: true,
                editable: true
            };





            /*+++++++++++++++++++++++++ Button Action ++++++++++++++++++++++++++++++++++++*/
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }


        function GlobalConfigController($scope, $mdDialog, $timeout, $q, $meteor) {
            /*+++++++++++++++++++++++++ TAB 1 ++++++++++++++++++++++++++++++++++++*/
            $scope.gconf = {};

            var getCalendarConf= function(){
                    var reqst = '/street_light.get_config';
                    $meteor.call('get', reqst, function(err,response){
                        console.log('data recieved : '+response);
                        var resp = JSON.parse(response.content);
                        console.log('==+++++++=== Success ==+++++++=='+resp);
                        if(resp.success){
                            console.log('============ Success ========'+resp);
                        }else{
                            console.log('++++++++++++++++++ERROR Occurd while Set RTC ++++++++++++++');
                        }

                    });
            }
            getCalendarConf();



            $scope.setPermitJoin = function(){
                var reqst = '/permit_node_join?seconds='+$scope.gconf.timeScnd;
                $meteor.call('get', reqst, function(err,response){
                    console.log('data recieved : '+response);
                    var resp = JSON.parse(response.content);
                    if(resp.success){
                        $scope.add_open = true;
                    }else{
                        console.log('++++++++++++++++++ERROR Occurd while Set Permit to join ++++++++++++++');
                    }

                });
            };


            $scope.setPermitJoin = function(){
                var reqst = '/change_rf_channel?channel='+$scope.gconf.rfChannel;
                $meteor.call('get', reqst, function(err,response){
                    console.log('data recieved : '+response);
                    var resp = JSON.parse(response.content);
                    if(resp.success){
                        $scope.add_open = true;
                    }else{
                        console.log('++++++++++++++++++ERROR Occurd while Changing RF Channel ++++++++++++++');
                    }

                });
            }; 


            $scope.setCalendar = function(){
                var reqst = '/street_light.set_config?calendar='+$scope.gconf.calendar?1:0;
                $meteor.call('get', reqst, function(err,response){
                    console.log('data recieved : '+response);
                    var resp = JSON.parse(response.content);
                    if(resp.success){
                        $scope.add_open = true;
                    }else{
                        console.log('++++++++++++++++++ERROR Occurd while Set Calendar ++++++++++++++');
                    }

                });
            };










            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
            
        }


        $meteor.autorun($scope, function() {
            $meteor.subscribe('devices', {
                limit: parseInt($scope.getReactively('perPage')),
                skip: (parseInt($scope.getReactively('page')) - 1) * parseInt($scope.getReactively('perPage'))

            }, $scope.getReactively('search')).then(function() {
                /*$scope.devices.forEach( function (party) {
                 party.onClicked = function () {
                 $state.go('partyDetails', {partyId: party._id});
                 };
                 });*/
            });

            $meteor.subscribe('deviceData', {}).then(function() {});


            var styles1 = [{
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#e0efef"}]
            }, {
                "featureType": "poi",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"hue": "#1900ff"}, {"color": "#c0e8e8"}]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{"lightness": 100}, {"visibility": "simplified"}]
            }, {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [{"visibility": "on"}, {"lightness": 700}]
            }, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#7dcdcd"}]}];
            var styles2 = [{
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#444444"}]
            }, {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [{"color": "#f2f2f2"}]
            }, {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "road",
                "elementType": "all",
                "stylers": [{"saturation": -100}, {"lightness": 45}]
            }, {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [{"visibility": "simplified"}]
            }, {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [{"visibility": "off"}]
            }, {
                "featureType": "water",
                "elementType": "all",
                "stylers": [{"color": "#46bcec"}, {"visibility": "on"}]
            }];


            $scope.map = {
                center: {
                    latitude: 6.933,
                    longitude: 80.305
                },
                options: {
                    styles: styles2,
                    maxZoom: 10
                },
                zoom: 8
            };
        });
    }]);

angular.module("socially").controller("gateway", ['$scope', '$meteor', '$rootScope', '$state', '$mdDialog','DataService','$mdMedia',
    function($scope, $meteor, $rootScope, $state, $mdDialog, DataService, $mdMedia) {
        $scope.gateway = $meteor.collection(function() {
            return Gateway.find({});
        });

        $meteor.autorun($scope, function() {
            $meteor.subscribe('gateway', {
                limit: parseInt($scope.getReactively('perPage')),
                skip: (parseInt($scope.getReactively('page')) - 1) * parseInt($scope.getReactively('perPage'))

            }, $scope.getReactively('search')).then(function() {
                /*$scope.devices.forEach( function (party) {
                 party.onClicked = function () {
                 $state.go('partyDetails', {partyId: party._id});
                 };
                 });*/
            });


        });
    }]);