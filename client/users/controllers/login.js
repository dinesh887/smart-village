angular.module("socially").controller("LoginCtrl", ['$meteor', '$state','$scope',
  function ($meteor, $state,$scope) {


    $scope.credentials = {
      email: '',
      password: ''
    };

    $scope.error = '';

    $scope.login = function () {
      $meteor.loginWithPassword($scope.credentials.email, $scope.credentials.password).then(
        function () {
          $state.go('deviceList');
        },
        function (err) {
          $scope.error = 'Login error - ' + err;
        }
      );
    };
  }
]);