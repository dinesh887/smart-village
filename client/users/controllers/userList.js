/**
 * Created by dineshpriyashantha on 12/5/15.
 */
angular.module("socially").controller("userList", ['$scope', '$meteor', '$rootScope', '$state', '$mdDialog','DataService','$mdMedia',
    function($scope, $meteor, $rootScope, $state, $mdDialog, DataService, $mdMedia) {
        $scope.usersList = $meteor.collection(Meteor.users,false).subscribe('usersList');

        $scope.showUser = function(ev, type, dev) {
            $mdDialog.show({
                controller: UserController,
                templateUrl: 'client/users/views/user.modal.ng.html',
                parent: angular.element(document.body),
                clickOutsideToClose:true,
                fullscreen: $mdMedia('sm') && $scope.customFullscreen,
                locals : {
                    type : type,
                    dev :dev
                }
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
            $scope.$watch(function() {
                return $mdMedia('sm');
            }, function(sm) {
                $scope.customFullscreen = (sm === true);
            });
        };

        function UserController($scope, $mdDialog, type, dev) {
            $scope.type = type;

            $scope.user = dev;

            $scope.credentials = {
                email:  $scope.user ?$scope.user.emails[0].address:'',
                password: '',
                username : $scope.user ?$scope.user.username:''
            };
            $scope.update =  function(){

                $meteor.call('updateUser',$scope.credentials);
            };

            $scope.addUser = function(){
                $meteor.createUser($scope.credentials).then(
                    function () {
                        $mdDialog.hide();
                    },
                    function (err) {
                       // vm.error = 'Registration error - ' + err;
                    }
                );
            };

            $scope.hide = function() {

            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };


        }
    }]);