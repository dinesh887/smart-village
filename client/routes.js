angular.module("socially").run(['$rootScope', '$state', function($rootScope, $state) {
  $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
    // We can catch the error thrown when the $requireUser promise is rejected
    // and redirect the user back to the main page
    if (error === 'AUTH_REQUIRED') {
      $state.go('parties');
    }
  });
}]);

angular.module("socially").config(['$urlRouterProvider', '$stateProvider', '$locationProvider',
  function ($urlRouterProvider, $stateProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $stateProvider
        .state('deviceList',{
          url: '/deviceList',
          templateUrl: 'client/parties/views/device-list.ng.html',
          controller: 'deviceList'
        })
        .state('parties', {
          url: '/parties',
          templateUrl: 'client/parties/views/parties-list.ng.html',
          controller: 'PartiesListCtrl'
        })
        .state('admin', {
          url: '/admin',
          templateUrl: 'client/parties/views/device-admin.ng.html',
          controller: 'deviceAdmin'
        })
        .state('gateway', {
          url: '/gateway',
          templateUrl: 'client/parties/views/device-admin.gateway.ng.html',
          controller: 'gateway'
        })
        .state('deviceListAdmin', {
          url: '/deviceListAdmin',
          templateUrl: 'client/parties/views/device-list-admin.ng.html',
          controller: 'deviceListAdmin'
        })
      //.state('partyDetails', {
      //  url: '/parties/:partyId',
      //  templateUrl: 'client/parties/views/party-details.ng.html',
      //  controller: 'PartyDetailsCtrl',
      //  resolve: {
      //    "currentUser": ["$meteor", function($meteor){
      //      return $meteor.requireUser();
      //    }]
      //  }
      //})

      .state('login', {
        url: '/login',
        templateUrl: 'client/users/views/login.ng.html',
        controller: 'LoginCtrl'
      })
      .state('register',{
        url: '/register',
        templateUrl: 'client/users/views/register.ng.html',
        controller: 'RegisterCtrl',
        controllerAs: 'rc'
      })

      .state('resetpw', {
        url: '/resetpw',
        templateUrl: 'client/users/views/reset-password.ng.html',
        controller: 'ResetCtrl',
        controllerAs: 'rpc'
      })
        .state('userList', {
        url: '/userList',
        templateUrl: 'client/users/views/user-list.ng.html',
        controller: 'userList',
        controllerAs: 'rpc'
      })
      .state('logout', {
        url: '/logout',
        resolve: {
          "logout": ['$meteor', '$state', function($meteor, $state) {
            return $meteor.logout().then(function(){
              $state.go('login');
            }, function(err){
              console.log('logout error - ', err);
            });
          }]
        }
      });

    $urlRouterProvider.otherwise("/login");
  }]);