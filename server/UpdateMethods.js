/**
 * Created by Heshanr on 10/15/2015.
 */
Meteor.methods({
    "deviceUpdate" : function () {
        console.log('deviceUpdate');
        var reqst = '/get_dev_list';
        Meteor.call('get', reqst, function(err,response){
            console.log('data recieved : '+response);
            var resp = JSON.parse(response.content);
            if(resp){
                var objects = resp.objects;
                _.each(objects,function(device){
                    if(device) {
                        Device.update({addr: device.addr},
                            device,
                            {upsert: true});
                    }
                });
            }

        });
    },
    "deviceDataUpdate":function(){
        console.log('deviceDataUpdate');
        var reqst = '/street_light.get_dev_list';
        Meteor.call('get', reqst, function(err,response){
            console.log('data recieved: '+response);
            var resp = JSON.parse(response.content);
            if(resp){
                var objects = resp.objects;
                _.each(objects,function(device){
                    if(device) {
                        DeviceData.update({dev: device.dev},
                            device,
                            {upsert: true});
                        device.timeStamp = new Date();
                        DeviceDataLog.insert(device);
                    }
                });
            }

        });
    },
    "deviceScheduleUpdate":function(){
        console.log('deviceScheduleUpdate');
        var reqst = '/street_light.get_dev_info';
        Meteor.call('get', reqst, function(err,response){
            console.log('data recieved: '+response);
            var resp = JSON.parse(response.content);
            if(resp){
                var objects = resp.objects;
                _.each(objects,function(device){
                    if(device) {
                        DeviceData.update({dev: device.dev},
                            {$set: {sch: device.sch}},
                            {upsert: true});
                    }
                });
            }
        });
    },

    "updateGateWayInfo": function(){
        console.log('Update Gateway');
        var reqst = '/info';
        Meteor.call('get', reqst, function(err,response){
            console.log('data received: '+response);
            var resp = JSON.parse(response.content);
            if(resp){
                var objects = resp.objects;
                _.each(objects,function(gateway){
                    if(gateway) {
                        Gateway.update({mac_address:gateway.mac_address},gateway,{upsert:true});
                    }
                });
            }

        });
    }
});