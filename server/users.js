Meteor.publish("users", function () {
  return Meteor.users.find({}, {fields: {emails: 1, profile: 1}});
});

Meteor.publish("usersList", function () {
  return Meteor.users.find({});
});

Meteor.methods({
  "updateUser" : function(user){
      var f_user = Meteor.users.findOne({'emails.address':user.email});
      var update = Meteor.users.update({'_id':f_user._id},
         {$addToSet:{
             "emails":[{"address":user.email}],
             "username" : user.username
         }});
    console.log(update);
  }
});
