/**
 * Created by Heshanr on 10/15/2015.
 */
SyncedCron.add({
    name: 'DBUpdate',
    schedule: function(parser) {
        // parser is a later.parse object
        return parser.text('every 1 minutes');
    },
    job: function() {
        Meteor.call('deviceUpdate');
        Meteor.call('deviceScheduleUpdate');
        Meteor.call('deviceDataUpdate');
    }
});