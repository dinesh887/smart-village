/**
 * Created by Heshanr on 10/21/2015.
 */
Meteor.publish("devices", function (options, searchString) {
    if (searchString == null)
        searchString = '';


    return Device.find({}, options);
});


Meteor.publish("deviceDataLog", function (options, searchString) {
    if (searchString == null)
        searchString = '';


    return DeviceDataLog.find({}, options);
});


Meteor.publish("deviceData", function (options, searchString) {
    if (searchString == null)
        searchString = '';


    return DeviceData.find({}, options);
});

Meteor.publish("gateway", function (options, searchString) {
    if (searchString == null)
        searchString = '';

    return Gateway.find({}, options);
});