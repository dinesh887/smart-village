/**
 * Created by Heshanr on 10/15/2015.
 */
Device = new Mongo.Collection("device");

Device.allow({
    insert: function (userId, party) {
        return true;
    },
    update: function (userId, party, fields, modifier) {
        return true;
    },
    remove: function (userId, party) {
        return true;
    }
});

