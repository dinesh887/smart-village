/**
 * Created by Heshanr on 10/15/2015.
 */
DeviceDataLog = new Mongo.Collection("deviceDataLog");

DeviceDataLog.allow({
    insert: function (userId, party) {
        return true;
    },
    update: function (userId, party, fields, modifier) {
        return true;
    },
    remove: function (userId, party) {
        return true;
    }
});