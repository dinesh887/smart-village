/**
 * Created by dineshpriyashantha on 1/3/16.
 */
Gateway = new Mongo.Collection("gateway");

Gateway.allow({
    insert: function (userId, party) {
        return true;
    },
    update: function (userId, party, fields, modifier) {
        return true;
    },
    remove: function (userId, party) {
        return true;
    }
});